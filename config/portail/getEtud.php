<?php

session_start();

/*
 * Définition des constantes
 */
const CSV_FILENAME_2019 = "Extraction_2019.csv";    // Nom du fichier de données
const CSV_FILENAME_2020 = "Extraction_2020.csv";    // Nom du fichier de données
const CSV_FILENAME_2021 = "Extraction_2021.csv";    // Nom du fichier de données
const CSV_FILENAME_2022 = "Extraction_2022.csv";    // Nom du fichier de données 2022/2023
const CSV_FILENAME_2023 = "Extraction_2023.csv";
const CSV_FILENAME_2024 = "Extraction_2024.csv";

const LINE_LENGTH_MAX = 9999;                 // Taille max d'une ligne csv (en nombre de caractères)
const CSV_DELIMITER = ";";                   // Separateur de champ (csv)
const CURRENT_YEAR = 2024;
/*
 * Définition des nom de champ CSV
 */
const CSV_NIP = "N_ETUDIANT";
const CSV_INE = "INE";
const CSV_ETAPE = "COD_ETP";
const CSV_ANNEE = "COD_ANU";
const CSV_NOM = "NOM";
const CSV_PRENOM = "PRENOM";
const CSV_DATE_DE_NAISSANCE = "DATE_DE_NAISSANCE";
const CSV_VILLE_DE_NAISSANCE = "VILLE_DE_NAISSANCE";
const CSV_CIVILITE = "CIVILITE";
const CSV_TEMOIN_PAIEMENT = "TEMOIN_PAIEMENT";
const CSV_ADRESSE_FIXE1 = "ADRESSE_FIXE1";
const CSV_ADRESSE_FIXE2 = "ADRESSE_FIXE2";
const CSV_ADRESSE_FIXE3 = "ADRESSE_FIXE3";
const CSV_ADRESSE_FIXE4 = "ADRESSE_FIXE4";
const CSV_ADRESSE_FIXE5 = "ADRESSE_FIXE5";
const CSV_ADRESSE_FIXE6 = "ADRESSE_FIXE6";
const CSV_TEL_PORTABLE = "TEL_PORTABLE";
//const CSV_MEL_PERSO = "MEL_PERSO";
const CSV_MEL_ULILLE = "MEL_ULILLE";
const CSV_BAC = "BAC";
const CSV_ANNEE_DU_BAC = "ANNEE_DU_BAC";
const CSV_BOURSE = "BOURSIER";


/*
 * Définition des champs XML
 */
const XML_FULLNAME = "fullname";
const XML_NIP = "nip";
const XML_ETAPE = "etape";
const XML_ANNEE = "annee";
const XML_INSCRIPTION = "inscription";
const XML_NOM = "nom";
const XML_PRENOM = "prenom";
const XML_SEXE = "gender";
const XML_NAISSANCE = "naissance";
const XML_VILLE_DE_NAISSANCE = "lieu_naissance";
const XML_MAIL = "mail";
//const XML_MAILPERSO = "mailperso";
const XML_ADDRESS = "address";
const XML_CODEP = "postalcode";
const XML_VILLE = "city";
const XML_PAYS = "country";
const XML_TELEPHONE = "phone";
const XML_LYCEE = "lycee";
const XML_BAC = "bac";
const XML_MENTION = "mention";
const XML_ANNEEBAC = "anneebac";
const XML_PAIEMENT = "paiementinscription";
const XML_BOURSE = "bourse";
const XML_INE = "ine";

const VAL_PAYE = "Insc. payee";
/*
 * Définition des nom des paramètres
 */
const PARM_NIP = "nip";
const PARM_ETAPE = 'etape';
const PARM_ANNEE = 'annee_apogee';
const PARM_NOM = "nom";
const PARM_PRENOM = "prenom";

/*
 * Ce module permet la connexion entre scodoc et des données exportées sous forme d'un csv (encodage UTF-8, délimiter: ';').
 * Le fichier d'origine peut être par exemple une exportation des données d'apogée.
 * La première ligne du fichier csv contient le nom des champs.
 * Les noms des champs attendus dans le csv sont des constantes CSV_xxxxx
 * Le fichier xlm produit est respecte les attentes de scodoc. Les noms des champs fournis pour scodoc sont des constantes XML_xxxxxx
 *  *
 * Le service getEtud est implémenté et accepte les paramètres etape, nip, nom et prénom.
 *
 * Implémentation.
 * Ce logiciel est assemblé selon 3 fonctionalités:
 * 1: Lecture des données (depuis csv) et mise à disposition (fonction load, getItem)
 * 2: Lecture des paramètres de la requète
 *  (fonctions )
 * 3: Recherche des lignes demandées et mise en forme XML des données
 *   (fonctions xmlShowPrams, xmlProduce, allXml, xmlByNip, xmlByNom, xmlByXit, value, field)
 *
 * Structure de données:
 *  $lib: table de traduction des noms de champs CSV_xxxx en numéro d'ordre
 *  $allData[0..] -> toutes les données personnelles (sous forme d'un tableau de structures - 1 structure part étudiant) sauf l'élément $allData[0] qui enregistre les noms de champs.
 *  $allNips[nip] ->  le no de l'étudiant de nip (nip)
 *  $allXits[xit] -> la liste des no des étudiants de l'étape xit
 *
*/

const CSV_SEXE = "sexe";
$lib = array();
$allData = array();
$allNips = array();
$allXits = array();

/*
 * Ajoute la ligne $row de la table $allData (déjà connue) dans les tableaux $allNips et $allXits (pour optimisation de recherche)
 */
function register($row) {
    global $allNips, $allXits;
    $nip = getValue($row, CSV_NIP);
    $xit = getValue($row, CSV_ETAPE);
    $annee = getValue($row, CSV_ANNEE);
    if (! isset($allNips[$nip]) || getValue($row, CSV_ANNEE) > getValue($allNips[$nip], CSV_ANNEE)) {
        $allNips[$nip] = $row;
    }
    if (! isset($allXits[$annee])) {      // Si l'annee' apogée n'existe pas déjà, on l'initialise
        $allXits[$annee] = array();
    }
    if (! isset($allXits[$annee][$xit])) {      // Si le couple (annee, code étape apogée) n'existe pas déjà, on l'initialise
        $allXits[$xit] = array();
    }
    $allXits[$annee][$xit][] = $row;
}

/*
 * Chargement des libellés de champs dans $lib
 */
function load_fields_name($data) {
    global $lib;
    for ($c=0; $c < count($data); $c++) {
        $lib[$data[$c]] = $c;
    }
//    error_log(print_r($lib, true));
}

/*
 * Chargement des données du fichier csv dans $allData et mise à jour des variables d'optimisation
 */
function load_csv($filename) {
    global $allData;
    if (($handle = fopen($filename, "r")) !== FALSE) {
        if (($data = fgetcsv($handle, LINE_LENGTH_MAX, CSV_DELIMITER)) !== FALSE) {
            load_fields_name($data);
        }
        $row = count($allData);
        while (($data = fgetcsv($handle, LINE_LENGTH_MAX, CSV_DELIMITER)) !== FALSE) {
            $allData[$row] = $data;
            register($row);
            $row++;
        }
        fclose($handle);
    }
}

/*
 * Lecture de la valeur du champ $csv_field pour l'étudiant ligne $row
 */
function getValue($row, $csv_field) {
    global $allData, $lib;
    $idx = $lib[$csv_field];
    if (! isset($allData[$row][$idx])) {
        echo 'erreur : '. $csv_field. ' : ' .$row;
        // error_log(">>>>>>>>>> ".$row);
    }
    return $allData[$row][$idx];
}

/*
 * Fonction de normalisation des nom-prenom:
 * Initialement prévue pour gérer la, casse, les encodages, les accents et les espaces.
 * On a gardé ici que le traitement de la casse et des espaces en début ou fin de chaîne.
 */
function canonize_name($nom, $prenom) {
    $nom = trim($nom);
    $prenom=trim($prenom);
//        $utf8 = array(
//            '/[áàâãªä]/u' => 'a',
//            '/[ÁÀÂÃÄ]/u' => 'A',
//            '/[ÍÌÎÏ]/u' => 'I',
//            '/[íìîï]/u' => 'i',
//            '/[éèêë]/u' => 'e',
//            '/[ÉÈÊË]/u' => 'E',
//            '/[óòôõºö]/u' => 'o',
//            '/[ÓÒÔÕÖ]/u' => 'O',
//            '/[úùûü]/u' => 'u',
//            '/[ÚÙÛÜ]/u' => 'U',
//            '/ç/' => 'c',
//            '/Ç/' => 'C',
//            '/ñ/' => 'n',
//            '/Ñ/' => 'N',
//            '//' => '-', // conversion d'un tiret UTF-8 en un tiret simple
//            '/[]/u' => ' ', // guillemet simple
//            '/[«»]/u' => ' ', // guillemet double
//            '/ /' => ' ' // espace insécable (équiv. à 0x160)
//        );
//        $sans_accent = preg_replace(array_keys($utf8), array_values($utf8), $nom."@".$prenom);
//        $sans_accent = $nom."@".$prenom;
    return strtolower($nom."@".$prenom);
}

/*
 * Utilitaires de génération du XML
 */

/*
 * Création d'un champ $xml_field de valeur $value
 */
function produce_value($etudiantXml, $xml_field, $value) {
    $etudiantXml->addChild($xml_field, $value);
}

/*
 * Création d'un champ $xml_field ayant pour valeur le champ $csv_field de l'étudiant $row
 */
function produce_field($etudiantXml, $row, $xmlfield, $csv_field) {
    produce_value($etudiantXml, $xmlfield, getValue($row, $csv_field));
}

/*
 * Ajout des données d'un étudiant dans le xml de sortie
 */
function produce_student($row) {
    global $xml_data;
    $etudiantXml = $xml_data->addChild('etudiant');
    produce_value($etudiantXml, XML_FULLNAME, getValue($row,CSV_NOM)." ".getValue($row, CSV_PRENOM));
    produce_field($etudiantXml, $row, XML_NIP, CSV_NIP);
    produce_field($etudiantXml, $row, XML_ETAPE, CSV_ETAPE);
    produce_field($etudiantXml, $row, XML_ANNEE, CSV_ANNEE);
    produce_field($etudiantXml, $row, XML_INSCRIPTION, CSV_ANNEE);
    produce_field($etudiantXml, $row, XML_NOM, CSV_NOM);
    produce_field($etudiantXml, $row, XML_PRENOM, CSV_PRENOM);
    if (getValue($row, CSV_CIVILITE) == "Mr") {
        produce_value($etudiantXml, XML_SEXE, "M");
    } else {
        produce_value($etudiantXml, XML_SEXE, "F");
    }
    produce_field($etudiantXml, $row, XML_NAISSANCE, CSV_DATE_DE_NAISSANCE);
    produce_field($etudiantXml, $row, XML_VILLE_DE_NAISSANCE, CSV_VILLE_DE_NAISSANCE);
    produce_field($etudiantXml, $row, XML_MAIL, CSV_MEL_ULILLE);
//    produce_field($etudiantXml, $row, XML_MAILPERSO, CSV_MEL_PERSO);
    produce_value($etudiantXml, XML_ADDRESS, getValue($row, CSV_ADRESSE_FIXE2)." ".getValue($row, CSV_ADRESSE_FIXE3));
    $codep_ville = explode(" ", trim(getValue($row, CSV_ADRESSE_FIXE5)), 2);
//    print_r(getValue($row, CSV_ADRESSE_FIXE5));
//    print_r($lib[CSV_ADRESSE_FIXE5]);
    produce_value($etudiantXml, XML_CODEP, $codep_ville[0]);
    if (isset($codep_ville[1])) {
        produce_value($etudiantXml, XML_VILLE, $codep_ville[1]);
    } else {
        produce_value($etudiantXml, XML_VILLE, "#ERR#");
	    error_log('Pb Adresse fixe5 avec etudiant NIP: '.getValue($row, CSV_NIP));
    }
    produce_field($etudiantXml, $row, XML_PAYS, CSV_ADRESSE_FIXE6);
    produce_field($etudiantXml, $row, XML_TELEPHONE, CSV_TEL_PORTABLE);
    // XML_LYCEE
    produce_field($etudiantXml, $row, XML_BAC, CSV_BAC);
    // XML_MENTION
    produce_field($etudiantXml, $row, XML_ANNEEBAC, CSV_ANNEE_DU_BAC);
    $paiement = getValue($row, CSV_TEMOIN_PAIEMENT);
    produce_value($etudiantXml, XML_PAIEMENT, ($paiement == VAL_PAYE ? "true" : "false"));
    produce_field($etudiantXml, $row, XML_BOURSE, CSV_BOURSE);
    produce_field($etudiantXml, $row, XML_INE, CSV_INE);
}

/*
 * Traitement d'une requête dur le nip
 */
function xmlByNip($nip) {
    global $allNips;
//    print("nip: ".$nip."\n");
    if (isset($allNips[$nip])) {
        $row = $allNips[$nip];
        produce_student($row);
    } else {
        http_response_code(404);
    }
}

/*
 * Traitement d'une requête sur le code étape (xit)
 */
function xmlByXit($xit, $annee) {
    global $allXits;
//    print(" annee: ".$annee.", etape: ". $xit."\n");
//    print_r($allXits[$annee][$xit]);
    if (isset($allXits[$annee]) && isset($allXits[$annee][$xit])) {
//        print("array exists.\n");
        if (count($allXits[$annee][$xit]) > 0) {
//            print(count($allXits[$annee][$xit]). "students found.\n");
            foreach ($allXits[$annee][$xit] as $row) {
                produce_student($row);
            }
        } else {
            http_response_code(404);
        }
    }
}

/*
 * Traoitement d'une requête sur le nom prénom:
 *
 */
function xmlByNom($nom, $prenom) {
    global $allData;
    $empty = true;
    $nom_param = canonize_name($nom, $prenom);
    $found = array();
    error_log("<!-- RECHERCHE: [".$nom_param."] -->");
    for ($row=0; $row < count($allData) ; $row++) {
        $nom_data = canonize_name(
            getValue($row, CSV_NOM),
            getValue($row, CSV_PRENOM)
        );
//        error_log( "<!-- COMPARE: [".$nom_data."] -->");
        if ($nom_data == $nom_param) {
//            error_log("<!-- TROUVE: ".$nom_data." -->");
            $empty = false;
            $nip = getValue($row, CSV_NIP);
            if (!isset($found[$nip]) || (getValue($row, CSV_ANNEE) > getValue($found[$nip], CSV_ANNEE))) {
                $found[$nip] = $row;
            }
        }
    }
    if ($empty) {
        http_response_code(404);
    } else {
        foreach ($found as $nip => $row) {
            produce_student($row);
        }
    }
}

/*
 * Traitement d'une requête globale:
 * Sans paramêtre, on envoie tous les étudiants disponibles.
 */
function allXml() {
    global $allData;
    for ($row=0; $row < count($allData) ; $row++) {
        produce_student($row);
    }
}

function xmlShowParms($xml) {
    $parms = $xml->addChild('parameters');
    foreach($_GET as $key => $value) {
        $parms->addChild($key, $value);
    }
}

function xml_output($xml) {
    if ($xml->children()->count() > 0) {
        header("Content-Type: text/xml; charset=ISO-8859-1");
//        header("Content-Type: text/xml; charset=UTF-8");
        print($xml->asXML());
    } else {
        http_response_code(404);
    }
}

/**
 * Vérifie si il y a un cache et le cherche, sinon va chercher les infos dans le CSV et creer le cache.
 * si $force est à vrai on  n utilise pas le cache et les fonctions de apcu ne devraient jamais être appelées
 */
function load_cache_if_exists($force=false) {
    global $allData;
    global $allNips;
    global $allXits;
    global $lib;
    if ($force || (!apcu_exists('allData') && !apcu_exists('allXits') && !apcu_exists('allNips'))) {
       // Chargement du fichier csv
        $allData = array();
        $allXits = array();
        $allNips = array();
        // load_csv(CSV_FILENAME_2019);
        // load_csv(CSV_FILENAME_2020);
        // load_csv(CSV_FILENAME_2021);
        // load_csv(CSV_FILENAME_2022);
        load_csv(CSV_FILENAME_2023);
        load_csv(CSV_FILENAME_2024);
        if (! $force) {
            apcu_store('allData', $allData);
            apcu_store('allXits', $allXits);
            apcu_store('allNips', $allNips);
            apcu_store('lib', $lib);
        }
    } else {
//        chargement du cache
        $allData = apcu_fetch('allData');
        $allXits = apcu_fetch('allXits');
        $allNips = apcu_fetch('allNips');
        $lib = apcu_fetch('lib');
    }
}


//print(count($allData). " records loaded\n");
//print(count($allXits[2019]). " XIT loaded (2019)\n");
//print(count($allXits[2020]). " XIT loaded (2020)\n");
//print(count($allXits[2021]). " XIT loaded (2021)\n");
//print(count($allXits[2022]). " XIT loaded (2022)\n");



//Si le fichier cache n'existe pas
load_cache_if_exists(true);
$xml_data = new SimpleXMLElement('<etudiants/>');   // Initialisation genérateur XML
if (array_key_exists(PARM_NIP, $_GET)) {             // Selon le type de requête, on appelle la fonction adhoc
     xmlByNip($_GET[PARM_NIP]);                            // Recherche sur le NIP
} elseif (array_key_exists(PARM_ETAPE, $_GET) /*&& array_key_exists(PARM_ANNEE, $_GET)*/) {
    if (isset($_GET[PARM_ANNEE])) {
        //        print(" annee: ".$_GET[PARM_ANNEE]."\n");
        //	print_r($_GET);
        xmlByXit($_GET[PARM_ETAPE], substr($_GET[PARM_ANNEE],0, 4));                          // Recherche sur le code étape apogée
//        print_r($lib);
    } else {
     //        print(" annee par defaut: 2020\n");
         xmlByXit($_GET[PARM_ETAPE], CURRENT_YEAR);                          // Recherche sur le code étape apogée
    }
} elseif (array_key_exists(PARM_NOM, $_GET)) {
     xmlByNom(urldecode($_GET[PARM_NOM]), urldecode($_GET[PARM_PRENOM]));        // Recherche sur le Nom-Prénom
} elseif (count($_GET) == 0) {
     allXml();
}
xmlShowParms($xml_data);                                  // Renvoi de la valeur des paramètres
xml_output($xml_data);                                    // Envoi du xml construit
