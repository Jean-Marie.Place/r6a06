<?xml version="1.0" encoding="UTF-8"?>
<etapes>
<chimie>
      <etape code="1BEA6T ">BUT1 Gestion des entreprises et des administrations - en apprentissage </etape>
      <etape code="2BEA9A ">BUT2 Gestion comptable, fiscale et financière - en apprentissage </etape>
      <etape code="3BEA9A ">BUT3 Gestion comptable, fiscale et financière - en apprentissage </etape>
      <etape code="2BEA9B ">BUT2 Gestion et pilotage des ressources humaines - en apprentissage </etape>
      <etape code="3BEA9B ">BUT3 Gestion et pilotage des ressources humaines - en apprentissage </etape>
      <etape code="2BEA9C ">BUT2 Gestion, entrepreneuriat et management d'activités - en apprentissage </etape>
      <etape code="3BEA9C ">BUT3 Gestion, entrepreneuriat et management d'activités - en apprentissage </etape>
      <etape code="1BEAGA ">BUT1 Gestion des entreprises et des administrations </etape>
      <etape code="2BEAGE ">BUT2 Gestion comptable, fiscale et financière </etape>
      <etape code="3BEAGE ">BUT3 Gestion comptable, fiscale et financière </etape>
      <etape code="1BEAGO ">BUT1 Gestion des entreprises et des administrations - réorientation </etape>
      <etape code="2BEAMG ">BUT2 Gestion, entrepreneuriat et management d'activités </etape>
      <etape code="3BEAMG ">BUT3 Gestion, entrepreneuriat et management d'activités </etape>
      <etape code="2BEAPP ">BUT2 Contrôle de gestion et pilotage de la performance </etape>
      <etape code="3BEAPP ">BUT3 Contrôle de gestion et pilotage de la performance </etape>
      <etape code="2BEARH ">BUT2 Gestion et pilotage des ressources humaines </etape>
      <etape code="3BEARH ">BUT3 Gestion et pilotage des ressources humaines </etape>
      <etape code="3MTAPN ">LP Collaborateur social et paie - en contrat de professionnalisation </etape>
      <etape code="3MTAPP ">LP Collaborateur social et paie - en apprentissage </etape>
      <etape code="3MTATA ">LP Collaborateur social et paie </etape>
</chimie>
<bio>
      <etape code="3BGMFY ">BUT3 Innovation pour l'industrie - en apprentissage </etape>
      <etape code="3BGMJW ">BUT3 Conception et production durable - en apprentissage </etape>
      <etape code="2BGMNO ">BUT2 Innovation pour l'industrie </etape>
      <etape code="3BGMNO ">BUT3 Innovation pour l'industrie </etape>
      <etape code="2BGMPI ">BUT2 Management de process industriel </etape>
      <etape code="3BGMPI ">BUT3 Management de process industriel </etape>
      <etape code="2BGMPN ">BUT2 Management de process industriel - en apprentissage </etape>
      <etape code="3BGMPN ">BUT3 Management de process industriel - en apprentissage </etape>
      <etape code="1BGMUC ">BUT1 Génie mécanique et productique </etape>
      <etape code="2BGMUR ">BUT2 Conception et production durable </etape>
      <etape code="3BGMUR ">BUT3 Conception et production durable </etape>
      <etape code="3CEPMI ">LP Mesure, instrumentation, contrôle, vision industrielle </etape>
      <etape code="3CEPMP ">LP Mesure, instrumentation, contrôle, vision industrielle - en apprentissage </etape>
      <etape code="3CEPVI ">LP Mesure, instrumentation, contrôle, vision industrielle - en contrat de professionnalisation </etape>
      <etape code="3CPREA ">LP éco-conception de produits innovants - en apprentissage </etape>
      <etape code="3CPRPR ">LP éco-conception de produits innovants </etape>
      <etape code="3MSGCI ">LP Conception et industrialisation de textiles innovants - en contrat de professionnalisation </etape>
      <etape code="3MSGSG ">LP Conception et industrialisation de textiles innovants </etape>
      <etape code="3MSGVG ">LP Conception et industrialisation de textiles innovants - en apprentissage </etape>
      <etape code=""></etape>
      <etape code="2BFQCS ">BUT2 Déploiement d'applications communicantes et sécurisées - en apprentissage </etape>
      <etape code="3BFQCS ">BUT3 Déploiement d'applications communicantes et sécurisées - en apprentissage </etape>
      <etape code="2BFQDA ">BUT2 Déploiement d'applications communicantes et sécurisées </etape>
      <etape code="3BFQDA ">BUT3 Déploiement d'applications communicantes et sécurisées </etape>
      <etape code="2BFQDS ">BUT2 Réalisation d'applications : conception, développement, validation - en apprentissage </etape>
      <etape code="3BFQDS ">BUT3 Réalisation d'applications : conception, développement, validation - en apprentissage </etape>
      <etape code="2BFQDV ">BUT2 Réalisation d'applications : conception, développement, validation </etape>
      <etape code="3BFQDV ">BUT3 Réalisation d'applications : conception, développement, validation </etape>
      <etape code="1BFQQU ">BUT1 Informatique </etape>
      <etape code="1TTITI ">DU1 Tremplin Tertiaire Informatique </etape>
</bio>
<QHS>
      <etape code="4QHSZZ">M1 Systèmes de management intégrés dans les organisations industrielles - en apprentissage</etape>
      <etape code="4QHSPR">M1 Systèmes de management intégrés dans les organisations industrielles - en contrat pro</etape>
      <etape code="4QHSHS">M1 Systèmes de management intégrés dans les organisations industrielles</etape>
      <etape code="5QHSZZ">M2 Systèmes de management intégrés dans les organisations industrielles - en apprentissage</etape>
      <etape code="5QHSPR">M2 Systèmes de management intégrés dans les organisations industrielles - en contrat pro</etape>
      <etape code="5QHSHS">M2 Systèmes de management intégrés dans les organisations industrielles</etape>
</QHS>
<chimie>
      <etape code="2BCHAA ">BUT2 Analyse, contrôle-qualité, environnement - en apprentissage </etape>
      <etape code="3BCHAA ">BUT3 Analyse, contrôle-qualité, environnement - en apprentissage </etape>
      <etape code="2BCHAN ">BUT2 Analyse, contrôle-qualité, environnement </etape>
      <etape code="3BCHAN ">BUT3 Analyse, contrôle-qualité, environnement </etape>
      <etape code="1BCHCA ">BUT1 Chimie - en apprentissage </etape>
      <etape code="1BCHCH ">BUT1 Chimie (tronc commun) </etape>
      <etape code="2BCHMA ">BUT2 Matériaux et produits formulés - en apprentissage </etape>
      <etape code="3BCHMA ">BUT3 Matériaux et produits formulés - en apprentissage </etape>
      <etape code="2BCHMT ">BUT2 Matériaux et produits formulés </etape>
      <etape code="3BCHMT ">BUT3 Matériaux et produits formulés </etape>
</chimie>
<mp>
      <etape code="2BMPAE ">BUT2 Mesures et analyses environnementales </etape>
      <etape code="2BMPAP ">BUT2 Mesures et analyses environnementales - en apprentissage </etape>
      <etape code="3BMPAP ">BUT3 Mesures et analyses environnementales - en apprentissage </etape>
      <etape code="2BMPYC ">BUT2 Matériaux et contrôles physico-chimiques </etape>
      <etape code="3BMPYC ">BUT3 Matériaux et contrôles physico-chimiques </etape>
      <etape code="2BMPYI ">BUT2 Techniques d'instrumentation </etape>
      <etape code="3BMPYI ">BUT3 Techniques d'instrumentation </etape>
      <etape code="1BMPYS ">BUT1 Mesures physiques </etape>
</mp>
<geii>
      <etape code="2BGLCG ">BUT2 Electricité et maîtrise de l'énergie - en apprentissage </etape>
      <etape code="3BGLCG ">BUT3 Electricité et maîtrise de l'énergie - en apprentissage </etape>
      <etape code="2BGLCT ">BUT2 Electricité et maîtrise de l'énergie </etape>
      <etape code="3BGLCT ">BUT3 Electricité et maîtrise de l'énergie </etape>
      <etape code="1BGLGE ">BUT1 Génie électrique et informatique industrielle </etape>
      <etape code="2BGLOM ">BUT2 Automatisme et informatique industrielle </etape>
      <etape code="3BGLOM ">BUT3 Automatisme et informatique industrielle </etape>
      <etape code="2BGLOP ">BUT2 Automatisme et informatique industrielle - en apprentissage </etape>
      <etape code="3BGLOP ">BUT3 Automatisme et informatique industrielle - en apprentissage </etape>
      <etape code="2BGLSE ">BUT2 Electronique et systèmes embarqués </etape>
      <etape code="3BGLSE ">BUT3 Electronique et systèmes embarqués </etape>
      <etape code="2BGLSU ">BUT2 Electronique et systèmes embarqués - en apprentissage </etape>
      <etape code="3BGLSU ">BUT3 Electronique et systèmes embarqués - en apprentissage </etape>
      <etape code="3IMRAP ">LP Robotique collaborative et mobile - en apprentissage </etape>
      <etape code="3IMRPR ">LP Robotique collaborative et mobile - en contrat de professionnalisation </etape>
      <etape code="3MSPGU ">LP Maintenance des transports guidés - en contrat d'apprentissage </etape>
      <etape code="3MSPPF ">LP Maintenance des transports guidés - en contrat de professionnalisation </etape>
      <etape code="3MSPSP ">LP Maintenance des transports guidés </etape>
      <etape code="1TSDSC ">DU1 Tremplin Secondaire </etape>
</geii>
<bio>
      <etape code="1BGBAL ">BUT1 Sciences de l'aliment et biotechnologie </etape>
      <etape code="2BGBAL ">BUT2 Sciences de l'aliment et biotechnologie </etape>
      <etape code="3BGBAL ">BUT3 Sciences de l'aliment et biotechnologie </etape>
      <etape code="1BGBBM ">BUT1 Biologie médicale et biotechnologie </etape>
      <etape code="2BGBBM ">BUT2 Biologie médicale et biotechnologie </etape>
      <etape code="3BGBBM ">BUT3 Biologie médicale et biotechnologie </etape>
      <etape code="1BGBBN ">BUT1 Diététique et nutrition </etape>
      <etape code="2BGBBN ">BUT2 Diététique et nutrition </etape>
      <etape code="3BGBBN ">BUT3 Diététique et nutrition </etape>
      <etape code="3BGBPU ">BUT3 Biologie médicale et biotechnologie - en apprentissage </etape>
      <etape code="3BGBVN ">BUT3 Sciences de l'aliment et biotechnologie - en apprentissage </etape>
      <etape code="3QSEAG ">LP Sécurité et qualité dans l'alimentation - en apprentissage </etape>
      <etape code="3QSECP ">LP Sécurité et qualité de l'alimentation - en contrat de professionnalisation </etape>
      <etape code="3QSEPS ">LP Sécurité et qualité dans les pratiques de soins </etape>
      <etape code="3QSEQA ">LP Sécurité et qualité dans l'alimentation </etape>
</bio>
<sd>
    <etape code="1BSTLC">BUT1 Statistique et informatique décisionnelle - en contrat professionnel</etape>
    <etape code="1BSTLL">BUT1 Statistique et informatique décisionnelle</etape>
    <etape code="1BSTLA">BUT1 Statistique et informatique décisionnelle - en apprentissage</etape>
    <etape code="2BSTEX">BUT2 Sciences des données : exploration et modélisation statistique</etape>
    <etape code="2BST6A">BUT2 Sciences des données : exploration et modélisation statistique - en apprentissage</etape>
    <etape code="2BST6C">BUT2 Sciences des données : exploration et modélisation statistique - en contrat pro</etape>
    <etape code="2BSTVI">BUT2 Sciences des données : visualisation, conception d'outils décisionnels</etape>
    <etape code="2BST6V">BUT2 Sciences des données : visualisation, conception d'outils décisionnels - en apprentissage</etape>
    <etape code="2BST6D">BUT2 Sciences des données : visualisation, conception d'outils décisionnels - en contrat pro</etape>
    <etape code="3BSTEX">BUT3 Sciences des données : exploration et modélisation statistique</etape>
    <etape code="3BST6A">BUT3 Sciences des données : exploration et modélisation statistique - en apprentissage</etape>
    <etape code="3BST6C">BUT3 Sciences des données : exploration et modélisation statistique - en contrat pro</etape>
    <etape code="3BSTVI">BUT3 Sciences des données : visualisation, conception d'outils décisionnels</etape>
    <etape code="3BST6V">BUT3 Sciences des données : visualisation, conception d'outils décisionnels - en apprentissage</etape>
    <etape code="3BST6D">BUT3 Sciences des données : visualisation, conception d'outils décisionnels - en contrat pro</etape>
</sd>
<mlt>
    <etape code="1BGTGI">BUT1 Management de la logistique et des transport</etape>
    <etape code="1BGTLO">BUT1 Management de la logistique et des transports- en apprentissage</etape>
    <etape code="1BGTRO">MLT - réorientation</etape>
    <etape code="2BGTMC">BUT2 Management de la mobilité et de la supply chain connectées</etape>
    <etape code="2BGTMU">BUT2 Management de la mobilité et de la supply chain durables</etape>
    <etape code="2BGTMP">BUT2 Management de la mobilité et de la supply chain connectées - en apprentissage</etape>
    <etape code="2BGTDP">BUT2 Management de la mobilité et de la supply chain durables - en apprentissage</etape>
    <etape code="3BGTMC">BUT3 Management de la mobilité et de la supply chain connectées</etape>
    <etape code="3BGTMU">BUT3 Management de la mobilité et de la supply chain durables</etape>
    <etape code="3BGTMP">BUT3 Management de la mobilité et de la supply chain connectées - en apprentissage</etape>
    <etape code="3BGTDP">BUT3 Management de la mobilité et de la supply chain durables - en apprentissage</etape>
	<etape code="1TRBTB">DU1 Tremplin Gestion Logistique Transport</etape>
</mlt>
<masters>
    <etape code="4QHSPR">M1 Systèmes de management intégrés dans les organisations industrielles - en contrat pro</etape>
    <etape code="4QHSHS">M1 Systèmes de management intégrés dans les organisations industrielles</etape>
    <etape code="5QHSPR">M2 Systèmes de management intégrés dans les organisations industrielles - en contrat pro</etape>
    <etape code="5QHSHS">M2 Systèmes de management intégrés dans les organisations industrielles</etape>
</masters>
</etapes>

